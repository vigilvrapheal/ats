package com.ibs.ats.iats.service;

import com.ibs.ats.iats.model.CandidateModel;
import com.ibs.ats.iats.model.Resume;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;

public interface ResumeParsingService {
    public Resume parseResume();
    public void splitSubtitle();

    List<CandidateModel> parseAndStoreResume(List<MultipartFile> file);
}
