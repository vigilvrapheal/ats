package com.ibs.ats.iats.service.impl;

import com.ibs.ats.iats.model.SkillSetDictionary;
import com.ibs.ats.iats.repository.SkillSetDictionaryRepo;
import com.ibs.ats.iats.service.ExtractorService;
import org.apache.commons.collections4.IteratorUtils;
import org.elasticsearch.index.query.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;

@Service
public class ExtractorImplementation implements ExtractorService {

    @Autowired
    private SkillSetDictionaryRepo skillSetDictionaryRepo;

    @Override
    public String emailExtractor(String content, String fullContent) {
        if(content.trim().isEmpty() && fullContent.trim().isEmpty()){
            return "";
        }
        final String regex = "([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\\.[a-zA-Z0-9_-]+)";
        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        Matcher matcher = pattern.matcher(content);
        StringBuilder stringBuilder = new StringBuilder();

        while (matcher.find()) {
            System.out.println("Email matched: " + matcher.group(0));
            stringBuilder.append(matcher.group(0)).append(", ");
        }

        if(stringBuilder.length() == 0){
            matcher = pattern.matcher(fullContent);
            while (matcher.find()) {
                System.out.println("Email matched: " + matcher.group(0));
                stringBuilder.append(matcher.group(0)).append(", ");
            }
        }

        return stringBuilder.length() != 0 ? stringBuilder.subSequence(0, stringBuilder.length()-2).toString() : "";
    }

    @Override
    public String skillsExtractor(String content, String fullContent) {
        List<String> skillsInResume = new ArrayList<>();
        if(content.contains(",")){
            List<String> commaSeparatedContents = Arrays.asList(content.split(","));
            commaSeparatedContents.stream().forEach(commaSeparatedContent -> {
                List<String> skills = Arrays.asList(commaSeparatedContent.split("\n"));
                skills.stream().forEach(skill -> {
                    String refinedSkill = skill.replaceAll("[!@$%^&*(),?\":\\{}|<>]", " ").replaceAll(" +", " ").trim().toLowerCase(Locale.ENGLISH);
                    QueryBuilder qb = matchQuery("name",refinedSkill);
                    List<SkillSetDictionary> matchedSkills = IteratorUtils.toList(skillSetDictionaryRepo.search(qb).iterator());
                    if(matchedSkills.size() != 0){
                        Boolean flag = false;
                        for(int i=0; i<matchedSkills.size(); i++){
                            if (matchedSkills.get(i).getName().contains(refinedSkill)){
                                skillsInResume.add(matchedSkills.get(i).getName());
                                flag = true;
                                break;
                            }
                        }

                        if(!flag){
                            skillsInResume.add(matchedSkills.get(0).getName());
                        }
                    }
                });
            });
        }
        return skillsInResume.stream().collect(Collectors.joining(","));
    }

    @Override
    public String mobileNumberExtractor(String content, String fullContent) {
        final String regex = "^((\\+|0)?\\d{1,3}[- ]?)?\\d{10}$";
        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        Matcher matcher = pattern.matcher(content);
        StringBuilder stringBuilder = new StringBuilder();

        while (matcher.find()) {
            System.out.println("Mobile number matched: " + matcher.group(0));
            stringBuilder.append(matcher.group(0)).append(", ");
        }

        if(stringBuilder.length() == 0){
            while (matcher.find()) {
                System.out.println("Mobile number matched: " + matcher.group(0));
                matcher = pattern.matcher(fullContent);
                stringBuilder.append(matcher.group(0)).append(", ");
            }
        }
        return stringBuilder.length() != 0 ? stringBuilder.subSequence(0, stringBuilder.length()-2).toString() : "";
    }

    @Override
    public String experienceExtractor(String expContainedContent) {
        final String regex = "(\\d{1,2} *( *|\\n)(year(s)?)( *|\\n)((and|&)( *|\\n)(\\d{1,2})( *)(month(s)?))?|\\d{1,2} *( *|\\n)month(s)?)";

        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        final Matcher matcher = pattern.matcher(expContainedContent);

        StringBuilder stringBuilder = new StringBuilder();

        while (matcher.find()) {
            System.out.println("Experience matched: " + matcher.group(0));
            stringBuilder.append(matcher.group(0).replaceAll("\n", " ").trim()).append(", ");
        }
        return stringBuilder.length() != 0 ? stringBuilder.subSequence(0, stringBuilder.length()-2).toString() : "";
    }
}
