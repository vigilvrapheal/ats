package com.ibs.ats.iats.service.impl;

import com.ibs.ats.iats.Utilities.Parser;
import com.ibs.ats.iats.common.CategoryKeys;
import com.ibs.ats.iats.model.CandidateModel;
import com.ibs.ats.iats.model.HeadingDictionary;
import com.ibs.ats.iats.model.Resume;
import com.ibs.ats.iats.repository.HeadingDictionaryRepo;
import com.ibs.ats.iats.repository.ResumeSearchRepository;
import com.ibs.ats.iats.service.ExtractorService;
import com.ibs.ats.iats.service.ResumeParsingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class ResumeParsingServiceImpl implements ResumeParsingService {

    @Autowired
    private ResumeSearchRepository resumeSearchRepository;

    @Autowired
    private ExtractorService extractorService;

    @Autowired
    private HeadingDictionaryRepo headingDictionaryRepo;

    @Override
    public Resume parseResume() {
        File folder = new File("/Users/vigilvrapheal/Desktop/resumePortal/resumeCollections/OneDrive_1_24-09-2020");
        for (final File fileEntry : folder.listFiles()) {
            Resume resume = Parser.parseResume(fileEntry);
            resumeSearchRepository.save(resume);
        }
        return null;
    }

    @Override
    public void splitSubtitle() {
        File folder = new File("/Users/vigilvrapheal/Desktop/resumePortal/resumeCollections/OneDrive_1_24-09-2020");
        for (final File fileEntry : folder.listFiles()) {
            try{
                Resume resume = Parser.parseResume(fileEntry);
                printTextFile(resume.getResumeName(), findAllSubtitles(resume.getResumeContent()));
            }catch (Exception e){
                e.printStackTrace();
                continue;
            }
        }
    }

    @Override
    public List<CandidateModel> parseAndStoreResume(List<MultipartFile> files) {
//        AtomicReference<CandidateModel> candidateModel = new AtomicReference<>(new CandidateModel());
        List<CandidateModel> candidateModels = new ArrayList<>();
        files.forEach(file -> {
//            CandidateModel candidateModel = new CandidateModel();
            try{
                Resume resume = Parser.parseResume(file);
                Map<String, String> splittedContent = findAllSubtitles(resume.getResumeContent());
                candidateModels.add(extractRequiredFieldsFromResume(resume, splittedContent));
                printTextFile(resume.getResumeName(), splittedContent);
            }catch (Exception e){
                e.printStackTrace();
            }
        });
        return candidateModels;
    }

    private Map<String, String> findAllSubtitles(String resumeContent){
        List<String> splittedResumeContents = Arrays.asList(resumeContent.split("\n"));

        Map<String, String> splitedContent = new LinkedHashMap<>();
        Set<Integer> matchedIndex = new LinkedHashSet<>();
        StringBuffer segmentContent = new StringBuffer();
        StringBuffer nextHeading = new StringBuffer();

        AtomicInteger index = new AtomicInteger();
        splittedResumeContents.stream().forEach(splittedResumeContent -> {
            index.incrementAndGet();
            splittedResumeContent = splittedResumeContent.trim().replaceAll(" +", " ");
            segmentContent.append(splittedResumeContent).append("\n");
            List<String> splitBySpace = Arrays.asList(splittedResumeContent.split(" "));
            if(!splittedResumeContent.isEmpty() && splitBySpace.size() <= 3 && Character.isUpperCase(splittedResumeContent.charAt(0))){
                List<HeadingDictionary> headingDictionary = headingDictionaryRepo.findByHeading(splittedResumeContent);
                if(headingDictionary.size() != 0) {
                    if(matchedIndex.size() == 0) {
                        splitedContent.put("TOP", segmentContent.toString());
                    }
                    else {
                        if(splitedContent.containsKey(nextHeading.toString())){
                            splitedContent.put(nextHeading.toString(), splitedContent.get(nextHeading.toString()).concat(segmentContent.toString()));
                        }else
                            splitedContent.put(nextHeading.toString(), segmentContent.toString());
                    }
                    segmentContent.setLength(0);
                    matchedIndex.add(index.get());
                    nextHeading.setLength(0);
                    nextHeading.append(headingDictionary.get(0).getCategory());
                }
            }
        });

        splitedContent.put(nextHeading.toString(), splitedContent.containsKey(nextHeading.toString()) ? splitedContent.get(nextHeading.toString()).concat(segmentContent.toString()) : segmentContent.toString());
        splitedContent.put("END", segmentContent.toString());
        if(splitedContent.containsKey("")){
            String content = splitedContent.get("TOP");
            String emptyKeyValue = splitedContent.get("");
            splitedContent.put("TOP",content + "\n" + emptyKeyValue);
            splitedContent.remove("");
        }
        return splitedContent;
    }

    private CandidateModel extractRequiredFieldsFromResume(Resume resume, Map<String, String> splittedContent){
        CandidateModel candidateModel = new CandidateModel();
        splittedContent.forEach((s, s2) -> {
            switch (CategoryKeys.valueOf(s.toUpperCase(Locale.ENGLISH))) {
                case ACHIEVEMENTS:
                    System.out.println("ACHIEVEMENTS");
                case EDUCATION:
                    System.out.println("EDUCATION");
                case EXPERIENCE:
                    System.out.println("EXPERIENCE");
                case ACTIVITIES:
                    System.out.println("ACTIVITIES");
                case OBJECTIVE:
                    System.out.println("OBJECTIVE");
                case PUBLICATION:
                    System.out.println("PUBLICATION");
                case SKILLS:
                    candidateModel.setSkills(extractorService.skillsExtractor(s2, resume.getResumeContent()));
                    break;
                case TOP:
                    candidateModel.setExperience(extractorService.experienceExtractor(s2));
                    candidateModel.setEmailId(extractorService.emailExtractor(s2, resume.getResumeContent()));
                    candidateModel.setPhoneNumber(extractorService.mobileNumberExtractor(s2, resume.getResumeContent()));
                    break;
                case END:
                    System.out.println();
                    break;
                default:
                    System.out.println("default");
            }
        });
        candidateModel.setFileName(resume.getResumeName());
        return candidateModel;
    }


    // dummy methods
    private void printTextFile(String fileName, Map<String, String> splittedContents) {

        try {
            StringBuilder content = new StringBuilder();
            splittedContents.forEach((key, value) -> {
                content.append("************************************************\n").append(key).append("\n************************************************\n\n");
                content.append(value).append("----------------------------------------------------------------------------------------------------------\n");
            });
            Path path = Paths.get("resumeText/");
            if(!Files.exists(path))
                Files.createDirectory(path);
            File myObj = new File("resumeText/" + fileName.replaceAll("\\.[a-zA-Z]*","").replaceAll("[!@#$%^&*(),.?\":\\{}|<>]","") + ".txt");
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
                FileWriter myWriter = new FileWriter(myObj);
                myWriter.write(content.toString());
                myWriter.close();
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
