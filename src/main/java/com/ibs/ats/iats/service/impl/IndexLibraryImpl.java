package com.ibs.ats.iats.service.impl;

import com.ibs.ats.iats.model.HeadingDictionary;
import com.ibs.ats.iats.model.SkillSetDictionary;
import com.ibs.ats.iats.repository.HeadingDictionaryRepo;
import com.ibs.ats.iats.repository.SkillSetDictionaryRepo;
import com.ibs.ats.iats.service.IndexLibrary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

@Service
public class IndexLibraryImpl implements IndexLibrary {

    private static final String PATH = "/Users/vigilvrapheal/Desktop/resumePortal/heading_lib/";

    @Autowired
    private HeadingDictionaryRepo indexDictionary;

    @Autowired
    private SkillSetDictionaryRepo skillSetDictionaryRepo;

    @Override
    public void indexExperience() {
        File file = new File(PATH + "4_experience_heading.txt");
        try {
            Scanner myReader = new Scanner(file);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                System.out.println(data);
                indexDictionary.save(new HeadingDictionary(data.trim().toLowerCase(Locale.ENGLISH), "Experience"));
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred in experience");
            e.printStackTrace();
        }
    }

    @Override
    public void indexObjective() {
        File file = new File(PATH + "5_Objective_heading.txt");
        try {
            Scanner myReader = new Scanner(file);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                indexDictionary.save(new HeadingDictionary(data.trim().toLowerCase(Locale.ENGLISH), "Objective"));
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    @Override
    public void indexSKills() {
        File file = new File(PATH + "2_skills.txt");
        try {
            Scanner myReader = new Scanner(file);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                indexDictionary.save(new HeadingDictionary(data.trim().toLowerCase(Locale.ENGLISH), "Skills"));
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    @Override
    public void indexPublication() {
        File file = new File(PATH + "6_Publication_heading.txt");
        try {
            Scanner myReader = new Scanner(file);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                indexDictionary.save(new HeadingDictionary(data.trim().toLowerCase(Locale.ENGLISH), "Publication"));
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    @Override
    public void indexAchievements() {
        File file = new File(PATH + "0_Achivements_headings.txt");
        try {
            Scanner myReader = new Scanner(file);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                indexDictionary.save(new HeadingDictionary(data.trim().toLowerCase(Locale.ENGLISH), "Achievements"));
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    @Override
    public void indexActivities() {
        File file = new File(PATH + "1_Activities_heading.txt");
        try {
            Scanner myReader = new Scanner(file);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                indexDictionary.save(new HeadingDictionary(data.trim().toLowerCase(Locale.ENGLISH), "Activities"));
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred in activities");
            e.printStackTrace();
        }
    }

    @Override
    public void indexEducation() {
        File file = new File(PATH + "3_Education_heading.txt");
        try {
            Scanner myReader = new Scanner(file);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                indexDictionary.save(new HeadingDictionary(data.trim().toLowerCase(Locale.ENGLISH), "Education"));
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred education");
            e.printStackTrace();
        }
    }

    @Override
    public void indexSkillSets() {
        File file = new File(PATH + "skills_set.txt");
        try {
            Scanner myReader = new Scanner(file);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                skillSetDictionaryRepo.save(new SkillSetDictionary(data.trim().toLowerCase(Locale.ENGLISH)));
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred skill set");
            e.printStackTrace();
        }
    }

    @Override
    public void indexSkillSetList(@RequestBody List<String> skills) {

        skills.forEach(skill -> skillSetDictionaryRepo.save(new SkillSetDictionary(skill.trim().toLowerCase(Locale.ENGLISH))));

    }
}
