package com.ibs.ats.iats.service;

import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface IndexLibrary {
    void indexExperience();
    void indexObjective();
    void indexSKills();
    void indexPublication();
    void indexAchievements();
    void indexActivities();
    void indexEducation();
    void indexSkillSets();
    void indexSkillSetList(@RequestBody List<String> skills);
}
