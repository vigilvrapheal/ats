package com.ibs.ats.iats.service;

import java.util.List;

public interface ExtractorService {
    String emailExtractor(String content, String fullContent);
    String skillsExtractor(String content, String fullContent);
    String mobileNumberExtractor(String content, String fullContent);

    String experienceExtractor(String content);
}
