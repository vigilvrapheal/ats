package com.ibs.ats.iats.Utilities;

import com.ibs.ats.iats.model.HeadingDictionary;
import com.ibs.ats.iats.model.Resume;
import com.ibs.ats.iats.repository.HeadingDictionaryRepo;
import com.ibs.ats.iats.repository.SkillSetDictionaryRepo;
import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

import static org.elasticsearch.index.query.QueryBuilders.termQuery;

public class Parser {
    private static Tika tika = null;

    static{
        tika = new Tika();
    }

    public static void main(String[] args) throws IOException, TikaException {
        Tika tika = new Tika();
        File file = new File("/Users/vigilvrapheal/Desktop/Resume/03082020-resume-vigil.pdf");
        FileInputStream fileInputStream =  new FileInputStream(file);
        String content = tika.parseToString(fileInputStream);
        System.out.println(content);
        System.out.println(file.getName());
    }

    public static Resume parseResume(File file) {

        String content = "";
        try{
            FileInputStream fileInputStream =  new FileInputStream(file);
            content = tika.parseToString(fileInputStream);
        }
        catch (Exception e) {
            e.printStackTrace();
            content = "";
        }

        return new Resume(file.getName(), content);
    }

    public static Resume parseResume(MultipartFile file) {

        String content = "";
        try{
            content = tika.parseToString(file.getInputStream());
        }
        catch (Exception e) {
            e.printStackTrace();
            content = "";
        }

        return new Resume(file.getOriginalFilename(), content);
    }
}
