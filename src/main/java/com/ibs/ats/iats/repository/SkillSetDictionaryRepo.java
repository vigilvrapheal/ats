package com.ibs.ats.iats.repository;

import com.ibs.ats.iats.model.SkillSetDictionary;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface SkillSetDictionaryRepo extends ElasticsearchRepository<SkillSetDictionary, String> {
    List<SkillSetDictionary> findByName(String name);
}
