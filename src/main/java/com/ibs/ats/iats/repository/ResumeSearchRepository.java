package com.ibs.ats.iats.repository;

import com.ibs.ats.iats.model.Resume;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ResumeSearchRepository extends ElasticsearchRepository<Resume, String> {
}
