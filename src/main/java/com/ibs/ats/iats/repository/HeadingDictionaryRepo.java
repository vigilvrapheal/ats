package com.ibs.ats.iats.repository;

import com.ibs.ats.iats.model.HeadingDictionary;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HeadingDictionaryRepo extends ElasticsearchRepository<HeadingDictionary, String> {
    List<HeadingDictionary> findByHeading(String header);
}
