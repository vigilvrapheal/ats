package com.ibs.ats.iats;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IAtsApplication {

	public static void main(String[] args) {
		SpringApplication.run(IAtsApplication.class, args);
	}

}
