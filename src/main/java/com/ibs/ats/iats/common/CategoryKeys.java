package com.ibs.ats.iats.common;

public enum CategoryKeys {
    ACHIEVEMENTS("achievements"),
    SKILLS("skills"),
    EDUCATION("education"),
    EXPERIENCE("experience"),
    ACTIVITIES("activities"),
    PUBLICATION("publication"),
    OBJECTIVE("objective"),
    TOP("top"),
    END("end");

    CategoryKeys(String achievements) {
    }
}
