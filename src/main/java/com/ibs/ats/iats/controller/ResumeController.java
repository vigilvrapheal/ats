package com.ibs.ats.iats.controller;

import com.ibs.ats.iats.model.CandidateModel;
import com.ibs.ats.iats.service.ExtractorService;
import com.ibs.ats.iats.service.IndexLibrary;
import com.ibs.ats.iats.service.ResumeParsingService;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;

@RestController
public class ResumeController {

    @Autowired
    private ResumeParsingService resumeParsingService;

    @Autowired
    private ExtractorService extractor;

    @Autowired
    private IndexLibrary indexLibrary;

    @GetMapping("/parseResume")
    public ResponseEntity parseResume(){
       return ResponseEntity.ok(resumeParsingService.parseResume());
    }

    @PostMapping("/parseResumesFromCandidates")
    public ResponseEntity parseResumeAndStore(@RequestParam("resumes") List<MultipartFile> file){
        return ResponseEntity.ok(resumeParsingService.parseAndStoreResume(file));
    }

    @GetMapping("/index/experience")
    public void indexExperience() {
        indexLibrary.indexExperience();
    }

    @GetMapping("/index/objective")
    public void indexObjective() {
        indexLibrary.indexObjective();
    }

    @GetMapping("/index/education")
    public void indexEducation() {
        indexLibrary.indexEducation();
    }

    @GetMapping("/index/skills")
    public void indexSkills() {
        indexLibrary.indexSKills();
    }

    @GetMapping("/index/activities")
    public void indexActivities() {
        indexLibrary.indexActivities();
    }

    @GetMapping("/index/achievements")
    public void indexAchievements() {
        indexLibrary.indexAchievements();
    }

    @GetMapping("/index/publication")
    public void indexPublication() {
        indexLibrary.indexPublication();
    }

    @GetMapping("/index/skillSets")
    public void indexSkillSets(){
        indexLibrary.indexSkillSets();
    }

    @PostMapping("/index/skillSetsList")
    public void indexSkillSetList(@RequestBody List<String> skills){
        indexLibrary.indexSkillSetList(skills);
    }

    @GetMapping("/getAllSubtitles")
    public void getAllSubtitle() {
        resumeParsingService.splitSubtitle();
    }

    // dummy methods

    @GetMapping("/parseSkills")
    public void parseSkills(){
        extractor.skillsExtractor("","");
    }
}
