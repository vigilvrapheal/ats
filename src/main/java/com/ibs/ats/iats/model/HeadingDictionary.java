package com.ibs.ats.iats.model;

import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "heading_dictionary")
public class HeadingDictionary {

    private String id;
    private String heading;
    private String category;

    public HeadingDictionary(String heading, String category) {
        this.heading = heading;
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
