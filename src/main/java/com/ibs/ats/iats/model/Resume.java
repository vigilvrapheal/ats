package com.ibs.ats.iats.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "resume", shards = 10, replicas = 2)
public class Resume {
    @Id
    private String id;
    private String resumeName;
    @Field(analyzer = "lowercase", type = FieldType.Text)
    private String resumeContent;

    public Resume(String resumeName, String resumeContent) {
        this.resumeName = resumeName;
        this.resumeContent = resumeContent;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResumeName() {
        return resumeName;
    }

    public void setResumeName(String resumeName) {
        this.resumeName = resumeName;
    }

    public String getResumeContent() {
        return resumeContent;
    }

    public void setResumeContent(String resumeContent) {
        this.resumeContent = resumeContent;
    }
}
